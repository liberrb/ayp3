#include <stdio.h>

void main()
{
    int enteros[3];
    int total = 0;

    printf("Ingrese 3 enteros:  ");
    scanf("%d %d %d", &enteros[0], &enteros[1], &enteros[2]);

    for (int i = 0; i < sizeof(enteros) / sizeof(int); i++)
    {
        total = total + enteros[i];
    }
    float avg = (float)total / (sizeof(enteros) / sizeof(int));
    printf("el promedio es: %f \n", avg);
}