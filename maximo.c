#include <stdio.h>

// obetner el maximo de una lista de numeros
int main()
{
    int numeros[] = {12, 30, 1, 0, 18, 123, 8, 555, 1, 8};
    int maximo = numeros[0];

    for (size_t i = 0; i < sizeof(numeros) / sizeof(int); i++)
    {
        if (maximo < numeros[i])
        {
            maximo = numeros[i];
        }
    }
    printf("el maximo es: %d \n", maximo);
    return maximo;
}