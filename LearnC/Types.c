// In the next exercise, you will need to create a program which prints out the sum of the numbers a, b, and c.
#include <stdio.h>

int main() {
  int a = 3;
  float b = 4.5;
  double c = 5.25;
  float sum;

  sum = a + b + c;

  printf("The sum of a, b, and c is %f.\n\n", sum);
  return 0;
}