#include <stdio.h>

void main()
{
    int numeros[] = {9, 100, 1, 8, 6, 2, 1, 99, 2};
    int minimo = numeros[0];

    for (size_t i = 0; i < sizeof(numeros) / sizeof(int); i++)
    {
        if (numeros[i] < minimo)
        {
            minimo = numeros[i];
        }
    }

    printf("El minimo es: %d \n", minimo);
}