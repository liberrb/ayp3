#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
    int opcion;

    while (opcion != 4)
    {
        printf("\nMENU:\n");
        printf("1. Opción 1\n");
        printf("2. Opción 2\n");
        printf("3. Opción 3\n");
        printf("4. Salir\n");
        printf("Seleccione una opcion: ");
        scanf("%d", &opcion);

        switch (opcion)
        {
        case 1:
            printf("###########\n");
            printf("ingreso a la opcion 1\n");
            printf("###########\n");
            break;
        case 2:
            printf("###########\n");
            printf("ingreso a la opcion 2\n");
            printf("###########\n");
            break;
        case 3:
            printf("###########\n");
            printf("ingreso a la opcion 3\n");
            printf("###########\n");
            break;
        case 4:
            printf("###########\n");
            printf("Saliendo.\n");
            sleep(1);
            printf("Saliendo..\n");
            sleep(1);
            printf("Saliendo...\n");
            sleep(1);
            system("clear");
            break;
        default:
            printf("###########\n");
            printf("numero de opcion incorrecto\n");
            printf("###########\n");
            break;
        }
    }

    return 0;
}